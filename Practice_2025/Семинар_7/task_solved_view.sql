DROP SCHEMA IF EXISTS sem_view CASCADE;
CREATE SCHEMA sem_view;

DROP TABLE IF EXISTS sem_view.organization;
CREATE TABLE sem_view.organization AS
SELECT
    1 AS id_org,
    'АО Тинькофф Банк' AS name_org
UNION
SELECT
    2,
    'X5 Retail Group'
UNION
SELECT
    3,
    'Сбер';

DROP TABLE IF EXISTS sem_view.teacher;
CREATE TABLE sem_view.teacher (id_teach, last_name, first_name, birth_date, salary_amt, id_org) AS
SELECT
    1, 'Роздухова', 'Нина', '1992-04-15', 15000.00, 1
UNION
SELECT
    2, 'Меркурьева', 'Надежда', '1995-03-12', 25000.00, 1
UNION
SELECT
    3, 'Халяпов', 'Александр', '1994-09-30', 17000.00, 2
UNION
SELECT
    4, 'Иванов', 'Иван', NULL, 100000.00, 3
UNION
SELECT
    5, 'Петров', 'Петр', NULL, 3000.00, 3
UNION
SELECT
    6, 'Сидоров', 'Василий', NULL, 17500.00, NULL
UNION
SELECT
    7, 'Данилов', 'Глеб', NULL, 2500.00, NULL;


/*
    1. Создать view – полную копию таблицы teacher;
*/

CREATE VIEW sem_view.teacher_v AS
SELECT *
FROM sem_view.teacher;

-- проверим, что есть в представлении
SELECT *
FROM sem_view.teacher_v;
/*
id_teach|last_name |first_name|birth_date|salary_amt|id_org|
--------+----------+----------+----------+----------+------+
       5|Петров    |Петр      |          |   3000.00|     3|
       6|Сидоров   |Василий   |          |  17500.00|      |
       3|Халяпов   |Александр |1994-09-30|  17000.00|     2|
       2|Меркурьева|Надежда   |1995-03-12|  25000.00|     1|
       7|Данилов   |Глеб      |          |   2500.00|      |
       4|Иванов    |Иван      |          | 100000.00|     3|
       1|Роздухова |Нина      |1992-04-15|  15000.00|     1|
*/

/*
    2. Создать view – копию таблицы teacher,
       за исключением строк, у которых нет связи с организацией;
*/

CREATE VIEW sem_view.teacher_with_org_v AS
SELECT *
FROM sem_view.teacher
WHERE id_org IS NOT NULL;

-- проверка
SELECT *
FROM sem_view.teacher_with_org_v;
/*
id_teach|last_name |first_name|birth_date|salary_amt|id_org|
--------+----------+----------+----------+----------+------+
       5|Петров    |Петр      |          |   3000.00|     3|
       3|Халяпов   |Александр |1994-09-30|  17000.00|     2|
       2|Меркурьева|Надежда   |1995-03-12|  25000.00|     1|
       4|Иванов    |Иван      |          | 100000.00|     3|
       1|Роздухова |Нина      |1992-04-15|  15000.00|     1|
*/

/*
    3. Создать view с полным списком преподавателей.
       Вместо id организации выводить ее название.
       Не включать в представление зарплату преподавателя;
*/

CREATE VIEW sem_view.teacher_org_v AS
SELECT id_teach, last_name, first_name, birth_date, name_org
FROM sem_view.teacher
LEFT OUTER JOIN
sem_view.organization
USING (id_org);

-- проверка
SELECT *
FROM sem_view.teacher_org_v;
/*
id_teach|last_name |first_name|birth_date|name_org        |
--------+----------+----------+----------+----------------+
       4|Иванов    |Иван      |          |Сбер            |
       5|Петров    |Петр      |          |Сбер            |
       1|Роздухова |Нина      |1992-04-15|АО Тинькофф Банк|
       2|Меркурьева|Надежда   |1995-03-12|АО Тинькофф Банк|
       3|Халяпов   |Александр |1994-09-30|X5 Retail Group |
       7|Данилов   |Глеб      |          |                |
       6|Сидоров   |Василий   |          |                |
*/

/*
    4. Создать view с полным списком преподавателей аналогично пункту (3).
       Фамилию и имя преподавателя объединить в одно поле.
       Поля назвать соответственно русским названиям
       – «Фамилия Имя», «Дата рождения», «Название организации»;
*/

CREATE OR REPLACE VIEW sem_view.teacher_org_name_v AS
SELECT id_teach, CONCAT(last_name, ' ', first_name) AS name, birth_date, name_org
FROM sem_view.teacher
LEFT OUTER JOIN
sem_view.organization
USING (id_org);

SELECT *
FROM sem_view.teacher_org_name_v;

/*
id_teach|name              |birth_date|name_org        |
--------+------------------+----------+----------------+
       4|Иванов Иван       |          |Сбер            |
       5|Петров Петр       |          |Сбер            |
       1|Роздухова Нина    |1992-04-15|АО Тинькофф Банк|
       2|Меркурьева Надежда|1995-03-12|АО Тинькофф Банк|
       3|Халяпов Александр |1994-09-30|X5 Retail Group |
       7|Данилов Глеб      |          |                |
       6|Сидоров Василий   |          |                |
*/

/*
    5. Написать вставку записи (на своё усмотрение) во view из пункта (1).
       Проверить, что новая запись появилась в исходной таблице;
*/

INSERT INTO sem_view.teacher_v
VALUES (8, 'Wick', 'John', '1964-09-02', 1500000.00, NULL);

SELECT *
FROM sem_view.teacher
WHERE last_name = 'Wick';
/*
id_teach|last_name|first_name|birth_date|salary_amt|id_org|
--------+---------+----------+----------+----------+------+
       8|Wick     |John      |1964-09-02|1500000.00|      |
*/

/*
    6. Написать удаление записи, вставленной в пункте (5), через view из пункта (1).
      Проверить, что запись удалилась из исходной таблицы;
*/

DELETE FROM sem_view.teacher_v
WHERE last_name = 'Wick';

SELECT *
FROM sem_view.teacher
WHERE last_name = 'Wick';
/*
id_teach|last_name|first_name|birth_date|salary_amt|id_org|
--------+---------+----------+----------+----------+------+
*/

/*
    7. Обновить дату рождения и у преподавателя id_teach = 4 (на любую) через view из пункта (1);
*/

UPDATE sem_view.teacher_v
SET birth_date = '1974-04-03'
WHERE id_teach = 4;

SELECT *
FROM sem_view.teacher
WHERE id_teach = 4;
/*
id_teach|last_name|first_name|birth_date|salary_amt|id_org|
--------+---------+----------+----------+----------+------+
       4|Иванов   |Иван      |1974-04-03| 100000.00|     3|
*/

/*
    8. Обновить id_org у преподавателя с id_teach = 4 на NULL через view из пункта (2).
       Проверить, что преподаватель пропал из view из пункта (2);
*/

UPDATE sem_view.teacher_with_org_v
SET id_org = NULL
WHERE id_teach = 4;

SELECT *
FROM sem_view.teacher_with_org_v
WHERE id_teach = 4;
/*
id_teach|last_name|first_name|birth_date|salary_amt|id_org|
--------+---------+----------+----------+----------+------+
*/

/*
    9. Пересоздать view и пункта (2) с условием [with local check option].
       Попробовать проделать те же манипуляции, что в пункте (8) на преподавателе id_teach = 5.
*/

CREATE OR REPLACE VIEW sem_view.teacher_with_org_v AS
SELECT *
FROM sem_view.teacher
WHERE id_org IS NOT NULL
WITH LOCAL CHECK OPTION;

UPDATE sem_view.teacher_with_org_v
SET id_org = NULL
WHERE id_teach = 5;
/*
SQL Error [44000]: ERROR: new row violates check option for view "teacher_with_org_v"
Detail: Failing row contains (5, Петров, Петр, null, 3000.00, null).
*/

/*
    10. Поверх view из предыдущего пункта создать view с [with cascaded check option].
        Проверить, что то же действие всё ещё кидает ошибку.
*/

CREATE OR REPLACE VIEW sem_view.teacher_with_org_cascaded AS
SELECT *
FROM sem_view.teacher_with_org_v
WITH CASCADED CHECK OPTION;

UPDATE sem_view.teacher_with_org_cascaded
SET id_org = NULL
WHERE id_teach = 5;

/*
[44000] ERROR: new row violates check option for view "teacher_with_org_v" 
Подробности: Failing row contains (5, Петров, Петр, null, 3000.00, null)
*/

/*
    11. Создать view со статистикой зарплат по организациям: 
        Среднее по организации и отличие от среднего, используя оконную функцию.
*/

CREATE OR REPLACE VIEW sem_view.teacher_salary_stats AS
SELECT
    id_teach,
    last_name,
    first_name,
    salary_amt,
    AVG(salary_amt) OVER (PARTITION BY id_org) AS avg_org_salary,
    salary_amt - AVG(salary_amt) OVER (PARTITION BY id_org) AS salary_diff
FROM sem_view.teacher;

SELECT * FROM sem_view.teacher_salary_stats;

/*
 id_teach | last_name   | first_name | salary_amt | avg_org_salary | salary_diff 
----------+-------------+------------+------------+----------------+-------------
        2 | Меркурьева  | Надежда    |      25000 |          20000 |        5000
----------+-------------+------------+------------+----------------+-------------
        1 | Роздухова   | Нина       |      15000 |          20000 |       -5000
----------+-------------+------------+------------+----------------+-------------
        3 | Халяпов     | Александр  |      17000 |          17000 |           0
----------+-------------+------------+------------+----------------+-------------
        5 | Петров      | Петр       |       3000 |           3000 |           0
----------+-------------+------------+------------+----------------+-------------
        7 | Данилов     | Глеб       |       2500 |          40000 |      -37500
----------+-------------+------------+------------+----------------+-------------
        4 | Иванов      | Иван       |     100000 |          40000 |       60000
----------+-------------+------------+------------+----------------+-------------
        6 | Сидоров     | Василий    |      17500 |          40000 |      -22500

*/


/*
    12. Создать view с учителями с зарплатой не ниже средней по организации (используте вложенный запрос) 
    (те, у кого не указана организация считаются в одной организации).
*/

CREATE OR REPLACE VIEW sem_view.teachers_above_avg AS
SELECT
    t.*
FROM sem_view.teacher t
WHERE salary_amt >= (
    SELECT AVG(salary_amt)
    FROM sem_view.teacher
    WHERE id_org = t.id_org OR (id_org IS NULL AND t.id_org IS NULL)
);

SELECT * FROM sem_view.teachers_above_avg;

/*
 id_teach | last_name   | first_name |  birth_date  | salary_amt | id_org 
----------+-------------+------------+--------------+------------+--------
        5 | Петров      | Петр       |              |       3000 |      3
----------+-------------+------------+--------------+------------+--------
        3 | Халяпов     | Александр  | 1994-09-30   |      17000 |      2
----------+-------------+------------+--------------+------------+--------
        2 | Меркурьева  | Надежда    | 1995-03-12   |      25000 |      1
----------+-------------+------------+--------------+------------+--------
        4 | Иванов      | Иван       | 1974-04-03   |     100000 |        

*/

/*
    (bonus) А теперь потренируемся с рекурсией: посчитаем числа Каталана:
    
    Зададим рекурренту:
    $C_0 = 1$, $C_n = \sum_{i=1}^{n} C_{i} + C_{n-i-1}$
    Которая выражается так:
    $C_0 = 1$, $C_n = \frac{2(2n - 1)}{n + 1}C_{n - 1}$

    Цель: создать рекурсивное представление для вычисления всех $C_k$, $k < n$.
*/

CREATE OR REPLACE VIEW sem_view.catalan_numbers(n, catalan) AS (
    SELECT
        0 AS n,
        1::NUMERIC AS catalan

    UNION ALL

    SELECT
        n + 1,
        (catalan * (2 * (2 * (n + 1) - 1))) / (n + 2)
    FROM catalan_numbers
    WHERE n < 15
);

SELECT n, catalan FROM catalan_numbers LIMIT 5;

/*
       n| catalan |
--------+---------+
       0|        1|
--------+---------+
       1|        1|
--------+---------+
       2|        2|
--------+---------+
       3|        5|
--------+---------+
       4|       14|
--------+---------+
*/