CREATE SCHEMA sem1;
-- DROP SCHEMA sem1 CASCADE;

CREATE TABLE sem1.suppliers (
    supplier_id SERIAL PRIMARY KEY,
    name VARCHAR(100),
    country VARCHAR(50)
);

INSERT INTO sem1.suppliers (name, country) VALUES
('Global Tech Supplies', 'USA'),
('Eco Products Inc.', 'Russia'),
('Quality Goods Co.', 'USA'),
('Mexican Imports LLC', 'Mexico'),
('Northern Lights Trading', 'Canada'),
('Sunshine Distributors', 'Russia'),
('Green Earth Supplies', 'USA');

CREATE TABLE sem1.customers (
    customer_id SERIAL PRIMARY KEY,
    name VARCHAR(100),
    country VARCHAR(50)
);

INSERT INTO sem1.customers (name, country) VALUES
('Alice Johnson', 'Russia'),
('Bob Smith', 'Canada'),
('Charlie Brown', 'USA'),
('Global Tech Supplies', 'UK'),
('Eco Products Inc.', 'Russia'),
('David Williams', 'USA'),
('Emily Davis', 'Mexico');

CREATE TABLE sem1.products (
    product_id SERIAL PRIMARY KEY,
    name VARCHAR(100),
    price DECIMAL(10, 2),
    supplier_id INT,
    FOREIGN KEY (supplier_id) REFERENCES sem1.suppliers(supplier_id)
);

INSERT INTO sem1.products (name, price, supplier_id) VALUES
('Apple iPhone 14', 999.99, 1),
('Samsung Galaxy S22', 899.99, 1),
('Sony WH-1000XM4 Headphones', 349.99, 2),
('Dell XPS 13 Laptop', 1299.99, 3),
('Bose QuietComfort Earbuds', 279.99, 3);

-- 1. Выведите всех поставщиков из `USA`.
-- 2. Для каждого продукта получите только его название и стоимость.
-- 3. Выведите имена всех задействованных лиц (поставщиков и заказчиков).
-- 4. Выведите имена всех задействованных лиц с повторениями.
-- 5. Найдите заказчиков, одновременно являющихся поставщиками.
-- 6. Найдите поставщиков, не являющихся заказчиками.
-- 7. Получите три самых дорогих продукта.
-- 8. Для каждого поставщика найдите самый дорогой продукт, который он продаёт (достаточно вывести id поставщика). 
-- 9. Для каждого продукта найдите название соответствующего поставщика (с использованием JOIN).
-- 10. Для каждого поставщика с более чем одним товаром найдите максимальную стоимость его товара.
