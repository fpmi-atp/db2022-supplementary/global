-- 1. Выведите всех поставщиков из `USA`
SELECT * FROM suppliers WHERE country = 'USA';

-- 2. Для каждого продукта получите только его название и стоимость.
SELECT name, price FROM products;

-- 3. Выведите имена всех задействованных лиц (поставщиков и заказчиков). 
SELECT name FROM suppliers
UNION
SELECT name FROM customers;

-- 4. Выведите имена всех задействованных лиц с повторениями.
SELECT name FROM suppliers
UNION ALL
SELECT name FROM customers;

-- 5. Найдите заказчиков, одновременно являющихся поставщиками.
SELECT name FROM suppliers
INTERSECT
SELECT name FROM customers;

-- 6. Найдите поставщиков, не являющихся заказчиками.
SELECT name FROM suppliers
EXCEPT
SELECT name FROM customers;

-- 7. Получите три самых дорогих продукта.
SELECT name, price
FROM sem1.products
ORDER BY price DESC
LIMIT 3;

-- 8. Для каждого поставщика найдите самый дорогой продукт, который он продаёт (достаточно вывести id поставщика).
SELECT DISTINCT ON (supplier_id) supplier_id, name, price
FROM sem1.products
ORDER BY supplier_id, price DESC;

-- 9. Для каждого продукта найдите название соответствующего поставщика (с использованием JOIN).
SELECT p.name AS product_name, s.name AS supplier_name
FROM sem1.products AS p
JOIN sem1.suppliers AS s
ON p.supplier_id = s.supplier_id;

-- 10. Для каждого поставщика с более чем одним товаром найдите максимальную стоимость его товара.
SELECT supplier_id, MAX(price)
FROM sem1.products
GROUP BY supplier_id
HAVING COUNT(*) > 1;

