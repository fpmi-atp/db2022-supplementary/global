-- NULL

-- 1. Найдите сотрудников с неопределённой зарплатой.
SELECT * FROM employees WHERE salary IS NULL;

-- 2. Выберите сотрудников с известной зарплатой.
SELECT * FROM employees WHERE salary IS NOT NULL;

-- 3. Выведите сотрудников по убыванию зарплаты.
SELECT * FROM employees ORDER BY salary DESC;

-- 4. Замените неопределённую зарплату на 0.
UPDATE employees SET salary = 0 WHERE salary IS NULL;


-- NaN

-- 5. Выведите все измерения.
SELECT * FROM measurements;

-- 6. Выведите все измерения без NaN значений.
SELECT * FROM measurements WHERE value != 'NaN';

-- 7. Выведите сумму всех измерений.
SELECT SUM(value) AS total_sum FROM measurements;

-- 8. Выведите сумму всех измерений исключая NaN значения.
SELECT SUM(value) AS total_sum_excluding_nan FROM measurements WHERE value != 'NaN';


-- Date

-- 9. Найдите все события первой половины 2025-го года.
SELECT * FROM events 
WHERE event_date BETWEEN '2025-01-01' AND '2025-06-30';

-- 10. Выведите количество событий в мае.
SELECT COUNT(*) AS event_count 
FROM events 
WHERE EXTRACT(MONTH FROM event_date) = 5;

-- 11. Для каждого месяца 2025-го года выведите количество событий.
SELECT EXTRACT(MONTH FROM event_date) AS month, COUNT(*) AS event_count 
FROM events 
WHERE EXTRACT(YEAR FROM event_date) = 2025 
GROUP BY month 
ORDER BY month;

-- 12. Получите список праздников, которые выпадают на пятницу.
SELECT * FROM events 
WHERE EXTRACT(DOW FROM event_date) IN (5); -- 0 - воскресенье, 6 - суббота

-- 13. Получените следующий праздник после текущей даты.
SELECT * FROM events 
WHERE event_date > CURRENT_DATE 
ORDER BY event_date 
LIMIT 1;
