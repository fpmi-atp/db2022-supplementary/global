-- CTE

--1. Создать таблицу согласно скрипту

--2. Вывести самый дорогой товар в каждой из категорий.

with max_price as
(
select group_id, max(price) as max_price
  from sem_5.products
 group by group_id
)
select p.product_id,
       p.product_name,
       p.price,
       p.group_id
  from sem_5.products p
  join max_price mp
    on p.group_id = mp.group_id
   and p.price = mp.max_price;

--3. Используя CTE вывести категории, имеющие более 6 товаров.

with group_counts as
(
select group_id, count(1) as group_cnt
  from sem_5.products
 group by group_id
)
select pg.group_id, pg.group_name
  from sem_5.product_groups pg
  join group_counts gc
    on pg.group_id = gc.group_id
 where gc.group_cnt > 6;

--4. Используя CTE, найти категории, в которых суммарная стоимость всех товаров превышает 5000.

with total_prices as (
    select group_id, sum(price) as total_price
    from sem_5.products
    group by group_id
)
select pg.group_id, pg.group_name, tp.total_price
from sem_5.product_groups pg
join total_prices tp on pg.group_id = tp.group_id
where tp.total_price > 5000;

--5. Используя CTE, вывести среднюю цену товаров в каждой категории, округлив её до целого числа.

with avg_prices as (
    select group_id, round(avg(price)) as avg_price
    from sem_5.products
    group by group_id
)
select pg.group_id, pg.group_name, ap.avg_price
from sem_5.product_groups pg
join avg_prices ap on pg.group_id = ap.group_id;

-- Рекурсивные запросы

--1. Написать запрос для вывода первых 10 членов геометрической прогресси с первым членом = 1 и множителем = 2.

with recursive geom(val, step) as
(
   values(1, 1)
    union all
   select val * 2, step + 1 from geom
    where step < 10
)
select val, step from geom;

--2.  Написать запрос на получение суммы арифметической прогрессии с шагом в 5, с первым членом 3 и последним 48.

with recursive arithmetic_series as (
    select 3 as term
    union all
    select term + 5
    from arithmetic_series
    where term + 5 <= 48
)
select sum(term) as sum_progression 
from arithmetic_series;

--3. Написать запрос для вывода первых 50 чисел Фибоначи.

with recursive fibonacci(val1, val2, step) as (
    values(1, 1, 1)
      union all
    select val2, val1 + val2, step + 1 from fibonacci
    where step < 50
)
select val1, step from fibonacci;

--4. Написать запрос, который выведет по порядку все дни, начиная с даты первого семинара по Базам Данных («2019-02-04») до сегодня.

with recursive seminar_dates as (
    select cast('2019-02-04' as date) as seminar_date
    union all
    select (seminar_date + interval '1 day')::date
    from seminar_dates
    where seminar_date < current_date
)
select seminar_date 
from seminar_dates;

--5. Написать запрос выводящий все даты, в которые были и будут семинары по Базам Данных, учитывая, что занятия начались «2019-02- 04», а закончатся до «2019-06-01» и проводятся в один и тот же день недели.

with recursive seminar_schedule as (
    select cast('2019-02-04' as date) as seminar_date
    union all
    select (seminar_date + interval '7 days')::date
    from seminar_schedule
    where seminar_date < date '2019-06-01'
)
select seminar_date 
from seminar_schedule;

--6. Создать таблицу согласно скрипту.

--7. Вывести для менеджера с employee_id = 2 всех подчиненных с помощью рекурсии.

with recursive emp as (
  select 
    employee_id, 
    manager_id, 
    full_name 
  from 
    employees 
  where 
    employee_id = 2 
  union 
  select 
    e.employee_id, 
    e.manager_id, 
    e.full_name 
  from 
    employees e 
    inner join emp s on s.employee_id = e.manager_id
)
select * from emp;

--Рекурсия для иерархии

--1. Для каждого `dep_id` вывести строку вида «Группа, Отдел, Управление, ...» к которым он относится.

with recursive department_hierarchy as (
    select 
        dep_id, 
        par_dep_id, 
        dep_name, 
        dep_name as hierarchy
    from sem_5.department
    where par_dep_id is null
    
    union all

    select 
        d.dep_id, 
        d.par_dep_id, 
        d.dep_name, 
        dh.hierarchy || ', ' || d.dep_name
    from sem_5.department d
    inner join department_hierarchy dh on d.par_dep_id = dh.dep_id
)
select dep_id, hierarchy 
from department_hierarchy
order by dep_id;

--2. Вывести `dep_id` структурных подразделений 5-го уровня вложенности, например, «Группа системного анализа, Отдел трансформации и загрузки данных, Управление хранилищ данных и отчетности, Департамент ИТ, Банк».

with recursive department_levels as (
    select 
        dep_id, 
        par_dep_id, 
        dep_name, 
        dep_name as hierarchy, 
        1 as level
    from sem_5.department
    where par_dep_id is null
    
    union all

    select 
        d.dep_id, 
        d.par_dep_id, 
        d.dep_name, 
        dl.dep_name || ', ' || d.dep_name AS hierarchy, 
        dl.level + 1
    from sem_5.department d
    inner join department_levels dl on d.par_dep_id = dl.dep_id
)
select dep_id, hierarchy
from department_levels
where level = 5
order by dep_id;
