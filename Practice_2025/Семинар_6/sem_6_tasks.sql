-- CTE

--1. Создать таблицу согласно скрипту

--2. Вывести самый дорогой товар в каждой из категорий.

--3. Используя CTE вывести категории, имеющие более 3 товаров.

--4. Используя CTE, найти категории, в которых суммарная стоимость всех товаров превышает 5000.

--5. Используя CTE, вывести среднюю цену товаров в каждой категории, округлив её до целого числа.

-- Рекурсия

--1. Написать запрос для вывода первых 10 членов геометрической прогресси с первым членом = 1 и множителем = 2.

--2.  Написать запрос на получение суммы арифметической прогрессии с шагом в 5, с первым членом 3 и последним 48.

--3. Написать запрос для вывода первых 50 чисел Фибоначи.

--4. Написать запрос, который выведет по порядку все дни, начиная с даты первого семинара по Базам Данных («2019-02-04») до сегодня.

--5. Написать запрос выводящий все даты, в которые были и будут семинары по Базам Данных, учитывая, что занятия начались «2019-02- 04», а закончатся до «2019-06-01» и проводятся в один и тот же день недели.

--6. Создать таблицу согласно скрипту.

--7. Вывести для менеджера с employee_id = 2 всех подчиненных с помощью рекурсии.

--Рекурсия для иерархии

--1. Для каждого `dep_id` вывести строку вида «Группа, Отдел, Управление, ...» к которым он относится.

--2. Вывести `dep_id` структурных подразделений 5-го уровня вложенности, например, «Группа системного анализа, Отдел трансформации и загрузки данных, Управление хранилищ данных и отчетности, Департамент ИТ, Банк».
